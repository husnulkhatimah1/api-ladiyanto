<?php

namespace App\Http\Controllers;

use App\Models\pesanan;
use Illuminate\Http\Request;

class PesananController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = pesanan::all();
        
        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request,[
            'nama' => 'required',
            'pemesanan' => 'required',
            'jumlahpesanan' => 'required | numeric'
        ]);

        $pesanan = pesanan::create($request->all());

        return response()->json($pesanan);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\pesanan  $pesanan
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pesanan = pesanan::find($id);

        return response()->json($pesanan);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\pesanan  $pesanan
     * @return \Illuminate\Http\Response
     */
    public function edit(pesanan $pesanan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\pesanan  $pesanan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pesanan = pesanan::find($id)->first();
        $this->validate($request,[
            'nama'=> 'required'
        ]);
        if($pesanan){
            $pesanan->update([
                'nama' => $request->nama,
                'pemesanan'=> $request->pemesanan,
                'jumlahpesanan'=> $request->jumlahpesanan
            ]);
            if ($pesanan){
                return response()->json("data ij");
            }
        }
        return response()->json("Data ini sudah di update");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\pesanan  $pesanan
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // pesanan::where('idpelanggan', $id)->delete();

        // return response()->json("Data ini Berhasil di hapus");
        $pesanan = pesanan::find($id);
        $pesanan->delete();
        return response()->json('Data berhasil di hapus');
    }
}
