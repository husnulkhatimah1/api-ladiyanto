<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class pesanan extends Model
{
    protected $table = "pesanans";

    protected $fillable = [
        'nama', 'pemesanan', 'jumlahpesanan'
    ];
}
